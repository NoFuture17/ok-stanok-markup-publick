//Глюки и Фиксы
/*
sourcemaps показывает не туда куда нужно: Chrome/DevTools/Network/DisableCache
Если все равно не работает:
Chrome/DevTools/Network/, right click on request table ‘Clear browser cache’, close files in ‘Sources’ tab.
Refresh source code.
Refresh browser.

node-sass@1.0.1: [Windows Error ] %1 is not a valid win32 application
грохнуть node_modules и npm install
*/

//Тормоза и resolve-url-loader
/*
 resolve-url-loader тормозит компиляцию в 1000(!) раз. Может когда-нить потом перестанет.
 Возможно нужно пообновлять библиотеки.
 раньше было так
 loader: ExtractTestWebpackPlugin.extract('style', 'css?sourceMap!resolve-url?sourceMap!sass?sourceMap'),
 Для npm-модулей поступать так:
 Способ 1: Указывать переменные для модулей напимер как здесь
 $slick-loader-path: "../node_modules/slick-carousel/slick/";
 $slick-font-path: "../node_modules/slick-carousel/slick/fonts/";
 Способ2:  импортировать css через ~. Именно css а не scss
 @import '~slick-carousel/slick/slick.css';
 @import '~slick-carousel/slick/slick-theme.css';
 Импортировать scss с относительными урлами по нормальному врядли выйдет
 здесь https://github.com/jtangelder/sass-loader#problems-with-url
 рекомендуют либо пользоваться переменными, либо супертормозным resolve-url-loader
 для less и stulys есть специальные опции. Для sass не было летом 2016

 Для scss в локальном проекте все url указывать относительно файла, который собирает остальные
 */


//devDependencies для webpack а так-же универсальные библиотеки
/*
 "babel-core": "^6.8.0",
 "babel-loader": "^6.2.4",
 "babel-polyfill": "^6.9.1",
 "babel-preset-es2015": "^6.6.0",
 "css-loader": "^0.23.1",
 "expose-loader": "^0.7.1",
 "extract-text-webpack-plugin": "^1.0.1",
 "file-loader": "^0.8.5",
 "html-loader": "^0.4.3",
 "imports-loader": "^0.6.5",
 "node-sass": "^3.7.0",
 "resolve-url-loader": "^1.4.3",
 "sass-loader": "^3.2.0",
 "sass-variable-loader": "0.0.3",
 "script-loader": "^0.7.0",
 "style-loader": "^0.13.1",
 "url-loader": "^0.5.7",
 "webpack": "^1.13.0",
 "webpack-dev-server": "^1.14.1",

 "whatwg-fetch": "^1.0.0",
 "normalize.css": "^4.2.0",
 "object-fit-images": "^2.5.3",
*/



var path = require('path');
var webpack = require('webpack');
var ExtractTestWebpackPlugin = require('extract-text-webpack-plugin');
var LiveReloadPlugin = require('webpack-livereload-plugin');

var isProd = process.env.NODE_ENV !== 'dev';
// var isProd = true;

//Конфиг для проекта
var dirAssets = './assets/';//root of SASS and ES6
//разбить данные, размазанные по конфигу на переменные и перетащить сюда, чтоб из проекта в проект не шариться по этому конфигу.
//Конфиг для проекта end

//Конфиг для dev-server
var devServerIP     = '127.0.0.1';  //default
var devServerPort   = '8080';       //default
var realServerBaseUrl = 'http://127.0.0.1';     //для devServer.proxy. //Для использования wds текущий проект должен быть доступен по этому адресу. Например алиас в OpenServer
var isDevServer = process.argv[1].indexOf('webpack-dev-server') !== -1;
console.log('------------Is webpack-dev-server runned: ' + isDevServer);
if (isDevServer){
    console.log('НЕ РАБОТАЙ С WDS! - СБОРКА БУДЕТ ГЛЮЧНОЙ! СМ. readme.md')
    console.log('DO NOT WORK WITH WDS! - BUILD WILL BE BUGGY! look readme.md')
}
var baseUrlForPublicPath = isDevServer
    ? 'http://' + devServerIP + ':' + devServerPort
    : '';
//Конфиг для dev-server end


var plugins = [
    new ExtractTestWebpackPlugin('[name].css', {allChunks: true,}),
    // new LiveReloadPlugin({appendScriptTag: true})
];

if (isProd){
    plugins = plugins.concat([
        new webpack.optimize.UglifyJsPlugin({mangle:   true, compress: {warnings: false}}),
    ]);
}

module.exports = {
    debug: !isProd,
    //То чего нет в https://webpack.github.io/docs/configuration.html#devtool1 (типы и производительность sourceMaps):
    //* eval не указывает в конструкторе какого класса ошибка при импорте и создании класса
    devtool: isProd ? false : 'cheap-module-source-map',
    entry: {
        index: ['./assets/index.js'],
        managers: ['./assets/managers.js'],
        'knowledge-base': ['./assets/knowledge-base.js'],
        'equipment': ['./assets/equipment.js'],
        'equipment2': ['./assets/equipment2.js'],
        'spares': ['./assets/spares.js'],
        'card': ['./assets/card.js'],
        'static': ['./assets/static.js'],
        'manager': ['./assets/manager.js'],
        'gallery': ['./assets/gallery.js'],
        'shopping-cart': ['./assets/shopping-cart.js'],
    },
    output: {
        path:       './public_html/builds',
        filename:   '[name].js',
        publicPath: baseUrlForPublicPath + '/builds/',
        chunkFilename: '[name].js'
    },
    plugins: plugins,
    module: {
        noParse: [/.node_modules(\\|\/)(gsap|jquery)./],
        loaders: [
            {
                test:   /\.js$/,
                loader: 'babel',
                include: [
                    path.resolve(__dirname, dirAssets),
                    /.node_modules(\\|\/)(jubilant-engine|delme|someOtherModule)./,
                ],
                query: {presets: ['es2015',]},
            },
            {
                test: /\.scss$/,
                loader: isProd
                            ? ExtractTestWebpackPlugin.extract('style', 'css!sass')
                            : !isDevServer
                                ? ExtractTestWebpackPlugin.extract('style', 'css?sourceMap!sass?sourceMap')
                                : 'style!css?sourceMap!sass?sourceMap',
            },
            { test: /\.html$/,                  loader: 'html'},
            // { test: /\.(svg)$/i,                loader: 'url', query: {limit: 5000},},   //other way
            { test: /\.(svg)$/i,                loader: 'url?limit=10000&name=[path][name].[ext]?[hash]',},
            { test: /\.(png|gif|jpe?g)$/i,      loader: "url?limit=10000&name=[path][name].[ext]?[hash]"},
            { test: /\.(woff|woff2|eot|ttf)$/,  loader: 'url-loader?limit=10000&name=[path][name].[ext]?[hash]'},
        ],
    },
    devServer: {
        //для livereload рекомендуется убрать из IDE Use "save write" (save changes to a temporary file first)
        host: devServerIP,
        port: devServerPort,
        contentBase: path.resolve(__dirname, 'public_html'),
        proxy: [{
            path: '**',
            target: realServerBaseUrl
        }],
    }
}
