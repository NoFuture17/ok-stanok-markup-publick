##Использование данной верстки

###PHP
* Сделать чтобы $_SERVER['DOCUMENT_ROOT'] == public_html
* composer install

###WebPack
* Установить Node.js
* Установить зависимые библиотеки для фронтэнда:  
  npm install
* запустить компиляцию фронтэнда через npm:
    npm run webpack
    или
    npm run watch
    
    npm run wds теперь использовать нельзя:
    в нем из-за того что стили подключаются яваскриптом в head после onReady
    часто глючат стили и скрипты. Иногда это смертельно (из-за этого пришлось отказаться от wds)
    Вместо wds теперь использовать npm run watch: для него установлен плагин webpack-livereload-plugin
    
    ОБЬЯЗАТЕЛЬНО в IDE добавить в исключения (mark directory as excluded)папку со зборками и 
    убрать в IDE Use "save write" (save changes to a temporary file first)
    иначе сборка будет работать медленнее и IDE сойдет с ума от постоянной переиндексации
    
    Позже нужно будет выпилить из webpack.config.js настройки для wds. Сейчас пока-что им не пользоваться ни в коем случае.
    
    для подробностей см. package.json  

Зафиксировать версии пакетов npm shrinkwrap --dev

>Исходники SASS и ES6 лежат в /assets  
Откомпилированные CSS и JS появляются в /public_html/builds

> если переменная окружения NODE_ENV !== 'dev',  
тогда WebPack минифицирует CSS и JS