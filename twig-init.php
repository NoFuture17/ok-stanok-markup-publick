<?php
require_once 'vendor/autoload.php';

function renderIt($templateName, $parsToTemplate = []){
    $loader = new Twig_Loader_Filesystem($_SERVER['DOCUMENT_ROOT'] . '/../templates');
    $twig = new Twig_Environment($loader);

    $ret = $twig->render($templateName, $parsToTemplate);
    echo $ret;
};
//just for test
