import './graceful-degradation.js';
import 'choices.js';
import 'nouislider';

import toggleScrolledFromTopClassToBody from '../jubilant-engine/toggleScrolledFromTopClassToBody';
toggleScrolledFromTopClassToBody({});


import MainNav from '../Includes/NavMain/MainNav.js';
MainNav();

import findError from '../Includes/find-error.js';
findError();

import header from '../Includes/Header/header.js';
header();

import NavGallery from '../Includes/NavGallerySE/NavGallerySE.js';
new NavGallery(document.querySelector('.nav-gallery-se'));

import listenScrollToHeader from '../jubilant-engine/listenScrollToHeader';
listenScrollToHeader(document.querySelector('.header-main-wrapper'));

import preloader from './preloader';
preloader();

import RightMenu from '../Includes/right-menu/right-menu';
RightMenu();

import Elevator from '../jubilant-engine/elevator';
new Elevator({
    nButton: document.querySelector('.elevator'),
    offsetTransparent: window.innerHeight / 2,
    offsetSolid: window.innerHeight,
});

import initFlickity from '../Includes/managers-hint-flickity/managers-hint-flickity';

import MansTie from '../Includes/managers-hint-tie/managers-hint-tie';
import MansSlider from '../Includes/managers-hint-slider/managers-hint-slider';

let mansTie = new MansTie();
let mansSlider = new MansSlider({});

mansTie.on('hide', mansSlider.show);
mansSlider.on('hide', mansTie.show);

initFlickity();

//проверки
setTimeout(()=>{
    // mansTie.status = 'hidden';
}, 300)
//проверки end
