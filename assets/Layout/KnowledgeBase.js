let isTouch = 'ontouchstart' in window;

let processFAQ =  () => {
    let items = document.querySelectorAll('.question-item');
    Array.from(items).forEach(item => {
        let question = item.querySelector('.question-item__question');

        if (!isTouch) {
            question.addEventListener('mouseenter', () => {
                question.classList.add('hover');
            });
            question.addEventListener('mouseleave', () => {
                question.classList.remove('hover');
            });
        }

        question.addEventListener('click', e => {
            if (item.classList.contains('question-item--open')) {
                item.classList.remove('question-item--open');
                question.classList.remove('hover');
            } else {
                item.classList.add('question-item--open')
                question.classList.add('hover');

            }
        });
    });
}

export default function KnowledgeBase(){
    processFAQ();
    
    
}