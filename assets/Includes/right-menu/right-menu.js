window.jQuery = require('jquery');
export default function RightNav() {
    jQuery(document).ready(function($) {
        let tieEl = $('.tie-element'),
            hintEl = $('.hint-element'),
            closeBtn = $('.cross-btn');

        tieEl.each(function() {
            $(this).on('click', function() {
                hintEl.addClass('hint-hide');
                tieEl.removeClass('tie-element--hidden')
                $(this).addClass('tie-element--hidden');
                $(this).next().removeClass('hint-hide');
                console.log($(this).next());
            })
        });
        closeBtn.each(function() {
            $(this).on('click', function() {
                $(this).parent().addClass('hint-hide').prev().removeClass('tie-element--hidden');
                console.log($(this).parent());
            })
        });

        let sendErrorBtn = $('.error-hint button'),
            popupErrorMsg = $('.popup-for-form-error-msg'),
            closeBtnPopup = popupErrorMsg.find('.je-alertino__cross');

        sendErrorBtn.submit(function(e) {
            popupErrorMsg.addClass('je-alertino--open');
            e.preventDefault();
        });
        closeBtnPopup.on('click', function() {
            popupErrorMsg.removeClass('je-alertino--open');
        })
    });
}