import noUiSlider from 'nouislider';
import commafy from '../../jubilant-engine/commafy';
import RadiosDisabler from '../../jubilant-engine/radios-disabler/radios-disabler';
// import makeRBSuncheckable from '../../jubilant-engine/makeRadiosUnceckable';
// import UnCheckableRBs from '../../jubilant-engine/UnCheckableRBs';

let formFilterEquipment = {
    _el: '',    //form element
    _chSel1: '',
    _noUiSlider1: '',
    _baseAction: '',
    _subButton: '',
    _slider: '',
    _listeners: {
        'change': [],
        'submit': [],
    },

    init: function () {
        this._el = document.querySelector('.form-filter-equipment');
        this._baseAction = this._el.action;
        this._subButton = document.querySelector('.form-filter-equipment__submit');
        this._reset = document.querySelector('.btn-clear-filter');
        this._initSelects();
        this._initSlider();
        this._initRadios();
        this._initResetButton();
        this._initSubmitButton();
        this._initChangeCbs();
        this._checkNoSold();

        this._el.addEventListener('submit', (e)=> {
            e.preventDefault();
            e.stopPropagation();
        });
    },
    _initRadios: function () {
        let trashTypeDisabler = new RadiosDisabler({
            nlRadios: this._el.querySelectorAll('.form-filter-equipment__trash-type input[type="radio"]'),
            nForm: this._el,
            cbUncheck: (rb)=>{
                this._procChandeCbs();
            }
        });
        let wareHouseDisabler = new RadiosDisabler({
            nlRadios: this._el.querySelectorAll('.form-filter-equipment__warehouse input[type="radio"]'),
            nForm: this._el,
            cbUncheck: (rb)=>{
                this._procChandeCbs();
            }
        });
        let stateDisabler = new RadiosDisabler({
            nlRadios: this._el.querySelectorAll('.form-filter-equipment__state input[type="radio"]'),
            nForm: this._el,
            cbUncheck: (rb)=>{
                this._procChandeCbs();

            }
        });
        
    },
    _initSlider: function () {
        let elMin = this._el.querySelector('.min > p');
        let elMax = this._el.querySelector('.max > p');

        var slider = document.querySelector('.form-filter-equipment__price-gap-slider');
        this._slider = slider;

        let start = [];
        let min = parseFloat(slider.dataset.min);
        let max = parseFloat(slider.dataset.max);
        let valueMin = parseFloat(slider.dataset.valueMin);
        let valueMax = parseFloat(slider.dataset.valueMax);

        if (valueMin > min && valueMin < max) {
            start[0] = valueMin;
        } else {
            start[0] = min
        }
        if (valueMax < max && valueMax > min) {
            start[1] = valueMax;
        } else {
            start[1] = max
        }

        noUiSlider.create(slider, {
            start: start,
            connect: true,  //линия межжду точками
            range: {
                'min': parseFloat(min),
                'max': parseFloat(max)
            }
        });

        //возможно здесь. Переустано
        // slider.noUiSlider.on('set', (e)=> {
        slider.noUiSlider.on('change', (e)=> {
            this._procChandeCbs();
            elMin.innerHTML = '₽ ' + commafy(Math.round([e[0]]));
            elMax.innerHTML = '₽ ' + commafy(Math.round([e[1]]));


        });
    },
    _initSelects: function () {
        let sel1 = this._el.querySelector('select.form-filter-equipment--equipment-type-select');

        let chSel1 = new Choices(sel1, {
            search: false,
            sortFilter: function(a, b) {
                if (a.id < b.id || a.value == 'empty') {
                    return -1;
                } else if (a.id > b.id || b.value == 'empty') {
                    return 1;
                } else {
                    return 0;
                }
            },
            callbackOnChange: (value, elSelect) => {
                this._el.reset();
                this._slider.noUiSlider.reset();
                this._resetAllCheckboxes();
                this._procChandeCbs();
            }
        });
        chSel1.containerOuter.classList.add('choices--blue');
        this._chSel1 = chSel1;

    },
    _initResetButton: function () {
        this._reset.setAttribute('href', this._baseAction);
        // this._reset.addEventListener('click', ()=> {
        //     this._el.reset();
        //     this._resetAllCheckboxes();
        //     this._chSel1.setValueByChoice('empty');
        //     this._procChandeCbs();
        //     this._slider.noUiSlider.reset();
        // });
    },
    _resetAllCheckboxes(){
        let cbxs = this._el.querySelectorAll('input[type=checkbox]');
//         console.log(cbxs)
        Array.from(cbxs).forEach((cbx)=>{
            // console.log('yo')
           cbx.checked = false; 
        });
    },
    _initChangeCbs: function () {
        let inputs = this._el.querySelectorAll('input');
        Array.from(inputs).forEach((input) => {
            this._initChangeCbsFor(input);
        })
        //для плагина choices колбэки вызываются в _initSelects
        //для nouislider соответственно в _initSlider

    },
    _initChangeCbsFor: function(input) {
        input.addEventListener('change', this._procChandeCbs.bind(this));
    },
    _procChandeCbs: function () {
        // console.log('_procChandeCbs')
        this._listeners['change'].forEach((cb)=> cb());
    },
    _initSubmitButton: function () {
        let href = this._el.querySelector('.form-filter-equipment__submit');
        var filter = this;
        href.addEventListener('click', (e)=> {
            e.preventDefault();

            var formData = filter._getFormData();
            var url = filter._updateUrl(formData);

            let input = document.createElement('input');
            input.type = 'hidden';
            input.name = 'price';
            input.value = filter._getPrice();
            filter._el.appendChild(input);

            filter._el.submit();
        });
    },
    _getFormData: function () {
        var formData = new FormData(this._el);
        formData.append('price', this._getPrice());
        return formData;
    },
    _getPrice: function () {
        return this._slider.noUiSlider.get()[0] + '-' + this._slider.noUiSlider.get()[1]
    },
    getVals: function (cb = (data)=> {
    }) {
        var formData = this._getFormData();
        this._updateUrl(formData);
        this._checkNoSold();
        // for (var [key, value] of formData.entries()) {console.log(key, value);}

        fetch(this._el.action, {
            method: 'POST',
            body: formData,
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            }
        }).then(function (res) {
            return res.json();
        }).then((data)=> {
            // console.log(data);
            cb(data)
        }).catch((err)=> {
            console.log(err);
        })
    },
    _checkNoSold: function () {
        let selectorAvailable = '.form-filter-equipment__warehouse input[type="radio"][value="0"]';
        let selectorNew = '.form-filter-equipment__state input[type="radio"][value="0"]';
        var disabledClass = 'je-radio--disabled';

        let inputAvailable = document.querySelector(selectorAvailable);
        let inputNew = document.querySelector(selectorNew);

        if (inputNew && inputAvailable) {
            if (inputAvailable.dataset.checked == 'true') {
                inputNew.parentNode.classList.add(disabledClass);
                inputNew.disabled = true;
            }else {
                inputNew.parentNode.classList.remove(disabledClass);
                inputNew.disabled = false;
            }

            if (inputNew.dataset.checked == 'true') {
                inputAvailable.parentNode.classList.add(disabledClass);
                inputAvailable.disabled = true;
            } else {
                inputAvailable.parentNode.classList.remove(disabledClass);
                inputAvailable.disabled = false;
            }
        }


    },
    _updateUrl: function(formData) {
        let urlPart = '';
        let categoryVal  = this._el.querySelector('select.form-filter-equipment--equipment-type-select').value;

        if (categoryVal && categoryVal != 'empty') {
            urlPart = urlPart + '/' + categoryVal;
        }

        let filterPart = '';

        let types = [];
        Array.from(this._el.querySelectorAll('.form-filter-equipment__tipy .ch-box input:checked')).forEach(function(item) {
            let val = item.attributes['value'].value;
            if (val) {
                types.push(item.attributes['value'].value);
            }
        });
        types.sort()
        types = types.join('_');
        if (types) {
            filterPart = filterPart + '/tipy_' + types;
        }

        let wasteInput = this._el.querySelector('.form-filter-equipment__trash-type input:checked');
        if (wasteInput && wasteInput.attributes['value'].value) {
            filterPart = filterPart + '/otkhody_' + wasteInput.attributes['value'].value;
        }

        let materials = [];
        Array.from(this._el.querySelectorAll('.form-filter-equipment__matter-select .ch-box input:checked')).forEach(function(item) {
            let val = item.attributes['value'].value;
            if (val) {
                materials.push(item.attributes['value'].value);
            }
        });
        materials.sort()
        materials = materials.join('_');
        if (materials) {
            filterPart = filterPart + '/materialy_' + materials;
        }

        let available = document.querySelector('[name="available"][data-checked="true"]');
        if (available) {
            filterPart = filterPart + '/available_' + available.value;
        }

        let newest = document.querySelector('[name="new"][data-checked="true"]');
        if (newest) {
            filterPart = filterPart + '/new_' + newest.value;
        }

        if (filterPart != '') {
            filterPart = '/f' + filterPart;
        }

        let newUrl = this._baseAction + urlPart + filterPart;
        this._subButton.href = newUrl;
        this._el.action = newUrl;
        return newUrl;
    },
    on: function (eName, cb) {
        this._listeners[eName].push(cb);
    },

}
export default formFilterEquipment;
