class ManagersHintSlider  {
    constructor({afterInitCb = undefined}){
        if (afterInitCb) this.on('afterInit', afterInitCb);
        
        this._node = document.querySelector('.managers-hint-slider');
        this._nCross = this._node.querySelector('.managers-hint-slider__cross');

        this.show = this.show.bind(this);
        this._hide = this._hide.bind(this);
        
        this._nCross.addEventListener('click', this._hide);
        this._processBodyClick();
        this._runListeners('afterInit');
        
    }
    // обработать клик по бади
    _processBodyClick(){
        let body = document.querySelector('body');
        this._node.addEventListener('click', (e) => {
            e.stopPropagation();
        })
        body.addEventListener('click', (e) => {
            this._hide();
        })
    }
    //Used listeners: hide
    //observer pattern
    on(eventName, cb){
        if (!this._listeners) this._listeners = {};
        if (!this._listeners[eventName]) this._listeners[eventName] = [];
        this._listeners[eventName].push(cb);
    }
    _runListeners(eName){
        if (!this._listeners || !this._listeners[eName]) return;
        this._listeners[eName].forEach((cb)=>{cb();})
    }
    //observer pattern end
    
    
    
    show(){
        this._node.classList.remove('managers-hint-slider--hidden');
    }
   _hide(){
       this._node.classList.add('managers-hint-slider--hidden');
       this._runListeners('hide');
    }
    
}
export default ManagersHintSlider;