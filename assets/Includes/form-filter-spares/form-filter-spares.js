let formFilterSpares = {
    _el: '',    //form element
    _baseAction: '',
    _chSel1:'',
    _chSel2:'',
    _subButton:'',
    _listeners: {
        'change': [],
        'submit': [],
    },

    init: function(){
        // this._initChangeCbs = this._initChangeCbs.bind(this);
        this._el = document.querySelector('.form-filter-spares');
        this._subButton = this._el.querySelector('.form-filter-spares__submit');
        this._baseAction = this._el.action;
        this._reset = document.querySelector('.btn-clear-filter');
        this._initSelects();
        this._initResetButton();
        this._initSubmitButton();
        this._initChangeCbs();

        this._el.addEventListener('submit', (e)=>{
            alert('hell');
            e.preventDefault();
            e.stopPropagation();
        });
    },
    _initSelects: function() {
        let sel1 = this._el.querySelector('select.form-filter-spares--equipment-type-select');
        let sel2 = this._el.querySelector('select.form-filter-spares--model-select');

        let chSel1 = new Choices(sel1, {search: false,
            sortFilter: function(a, b) {
            if (a.id < b.id || a.value == 'empty') {
                    return -1;
                } else if (a.id > b.id || b.value == 'empty') {
                    return 1;
                } else {
                    return 0;
                }
            },
          callbackOnChange: (value, elSelect) => {
            this._el.reset();
            this._chSel2.setValueByChoice('empty');
            this._procChandeCbs();
            this._updateSelect2(value)
          }
        });
        chSel1.containerOuter.classList.add('choices--blue');
        this._chSel1 = chSel1;

        let chSel2 = new Choices(sel2, {search: false,
            sortFilter: function(a, b) {
                if (a.id < b.id || a.value == 'empty') {
                    return -1;
                } else if (a.id > b.id || b.value == 'empty') {
                    return 1;
                } else {
                    return 0;
                }
            },
            callbackOnChange: (value, elSelect) => {this._procChandeCbs();}
        });
        chSel2.containerOuter.classList.add('choices--blue');
        this._chSel2 = chSel2;
    },
    _initResetButton: function() {
        this._reset.setAttribute('href', this._baseAction);
        // this._reset.addEventListener('click', ()=>{
        //     this._el.reset();
        //     this._chSel1.setValueByChoice('empty');
        //     this._chSel2.setValueByChoice('empty');
        //     this._procChandeCbs();
        // });
    },
    _initSubmitButton: function(){
        // let href = this._el.querySelector('.form-filter-spares__submit');
        // href.addEventListener('click', (e)=>{
        //    e.preventDefault();
        //     this._listeners['submit'].forEach((cb)=>cb());
        // });
    },
    _initChangeCbs: function() {
        let inputs = this._el.querySelectorAll('input');
        Array.from(inputs).forEach((input) => {
            input.addEventListener('change', this._procChandeCbs.bind(this));
        })
        //для плагина choices колбэки вызываются в _initSelects

    },
    getVals: function(cb = (data)=>{}) {
        // console.log('типа отправка формы и получение данных1');
        var formData = new FormData(this._el);
        this._updateUrl(formData);
        fetch(this._el.action,  {
            method: 'POST',
            body: formData,
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            }
        }).then(function(res){
            return res.json();
        }).then((data)=> {
            // console.log(data);
            cb(data)
        }).catch((err)=>{
            console.log(err);
        })
    },
    _procChandeCbs: function() {
        this._listeners['change'].forEach((cb)=> cb());
    },
    _updateSelect2: function(value) {
        let url = this._chSel2.passedElement.dataset['updateUrl'];
        var formData = new FormData(this._el);
        if (url) {
            fetch(url,  {
                method: 'POST',
                body: formData,
                headers: {
                    'X-Requested-With': 'XMLHttpRequest'
                }
            }).then(function(res){
                return res.json();
            }).then((data)=> {
                this._chSel2.clearStore();
                this._chSel2.setChoices(data, 'value', 'label', true);
            }).catch((err)=>{
                console.log(err);
            });
        }
    },
    _updateUrl: function(formData) {
        let urlPart = '';
        let zapNames = [];
        Array.from(this._el.querySelectorAll('.form-filter-spares__spares-ch-boxes .ch-box input:checked')).forEach(function(item) {
            zapNames.push(item.attributes['name'].value);
        });
        zapNames.sort()
        let zapVal = zapNames.join('_');
        if (zapVal) {
            urlPart = urlPart + '/zapchasti_' + zapVal;
        }

        let typeVal = this._el.querySelector('select.form-filter-spares--equipment-type-select').value;
        if (typeVal && typeVal != 'empty') {
            urlPart = urlPart + '/kategoriya_' + typeVal;

            let modelVal = this._el.querySelector('select.form-filter-spares--model-select').value;
            if (modelVal && modelVal != 'empty') {
                urlPart = urlPart + '/model_' + modelVal;
            }
        }

        if (urlPart != '') {
            urlPart = '/f' + urlPart;
        }

        this._subButton.href = this._baseAction + urlPart;
        this._el.action = this._baseAction + urlPart;
    },
    on: function(eName, cb){
        this._listeners[eName].push(cb);
    },

}
export default formFilterSpares;
