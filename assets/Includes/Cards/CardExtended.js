let cards;

function ProcessSimple(card){
    card.addEventListener('click', (e)=>{

        window.location = card.getAttribute('data-href');
    })
}

function ProcessExtended(card, linksExt){
    // console.log('has');
    var isTouch = 'ontouchstart' in window;
    if (isTouch) {
        ProcessExtendedTouch(card, linksExt);
    } else {
        ProcessExtendedNoTouch(card, linksExt);
    }

}

function ProcessExtendedTouch(card, linksExt){


    card.addEventListener('click', () => {
        // console.log('onElement');
        card.classList.add('active');
    })
}
function ProcessExtendedNoTouch(card, linksExt){
    card.addEventListener('mouseenter', () => {
        card.classList.add('active');
    });

    card.addEventListener('mouseleave', () => {
        card.classList.remove('active');
    });
}

function CardExtended(){
    cards = document.querySelectorAll('.card-extended');
    var isTouch = 'ontouchstart' in window;
    if (isTouch) {
        document.body.addEventListener('click', function(){
            // console.log('onBody');
            Array.from(cards).forEach(card => {
                card.classList.remove('active');
            });
        }, true)
    }



    Array.from(cards).forEach((card) => {
        let linksExt = card.querySelector('.card-extended__links-extended');
        if (linksExt) {
            ProcessExtended(card, linksExt);
        } else {
            ProcessSimple(card);
        }
    })
}

export default CardExtended;