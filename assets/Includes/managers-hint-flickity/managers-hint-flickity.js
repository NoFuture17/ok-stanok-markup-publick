import Flickity from 'flickity';

function init(){
    
    var flkty = new Flickity( '.managers-hint-flickity', {
        cellSelector: '.managers-hint-flickity-slide',
        accessibility: false,   //control with keyboard arrows. def: true
        // cellAlign: 'center', //left, right
        // contain: true
        wrapAround: true,    //зациклить
        lazyLoad: 2,          //2 изображения слева и справа
        autoPlay: 6000,
        pageDots: false
    });
    
}

export default init;