export default function NavGallery(){
    //есть ситуации, когда анимация остается без единого листнера
    let isMouseOnElement = false;
    let timeAnimation = 500;
    let navGal = document.querySelector('.nav-gallery-se');
    
    function activate() {
        console.dir('acivate');
        navGal.removeEventListener('mouseleave', deactivate);
        
        navGal.classList.remove('nav-gallery-se_state1');
        navGal.classList.add('nav-gallery-se_state2');
        setTimeout(function(){
            navGal.classList.remove('nav-gallery-se_state2');
            navGal.classList.add('nav-gallery-se_state3');
            setTimeout(function(){
                navGal.classList.remove('nav-gallery-se_state3');
                navGal.classList.add('nav-gallery-se_state4');
                setTimeout(function(){
                    if (!isMouseOnElement) deactivate();
                    navGal.addEventListener('mouseleave', deactivate);
                },timeAnimation);
            }, timeAnimation);
        }, timeAnimation);
    }
    function deactivate() {
        console.dir('deacivate');
        navGal.removeEventListener('mouseenter', activate);
        navGal.classList.remove('nav-gallery-se_state4');
        navGal.classList.add('nav-gallery-se_state3');
        setTimeout(function(){
            navGal.classList.remove('nav-gallery-se_state3');
            navGal.classList.add('nav-gallery-se_state2');
            setTimeout(function(){
                navGal.classList.remove('nav-gallery-se_state2');
                navGal.classList.add('nav-gallery-se_state1');
                setTimeout(function(){
                    if (isMouseOnElement) activate();
                    navGal.addEventListener('mouseenter', activate); 
                });
            }, timeAnimation);
        }, timeAnimation);


    }
    
    navGal.addEventListener('mouseenter', activate);
    navGal.addEventListener('mouseleave', deactivate);
    
    navGal.addEventListener('mouseenter', ()=> {
        isMouseOnElement = true;
    });
    navGal.addEventListener('mouseleave', ()=> {
        isMouseOnElement = false;
    });
}