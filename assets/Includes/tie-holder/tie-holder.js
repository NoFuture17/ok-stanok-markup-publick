import nodeFromString from '../../jubilant-engine/createNodeFromHTMLString';

let template = `<div class="tie-holder">
    <div class="tie-holder__cross"></div>
    <div class="tie-holder__content"></div>
    <div class="tie-holder__triangle"></div>
</div>`;

class TieHolder {

    constructor() {
        this.show = this.show.bind(this);
        this.hide = this.hide.bind(this);
        this._initEl();
        this._initObserver();
        this._initListeners();
    }

    _initEl() {
        let oldEl = document.querySelector('.tie-holder');
        let cont = oldEl.innerHTML;
        let newEl = nodeFromString(template);
        newEl.querySelector('.tie-holder__content').innerHTML = cont;
        oldEl.parentNode.replaceChild(newEl, oldEl);

        this._el = document.querySelector('.tie-holder');
        this._cross = this._el.querySelector('.tie-holder__cross');
    }
    _initObserver() {
        this._observer = {
            'afterhide': []
        }
    }

    _initListeners() {
        this._cross.addEventListener('click', this.hide);
    }

    show() {
        this._el.classList.add('tie-holder--visible');
        this._el.dispatchEvent(new Event('scroll-to-header', {'bubbles': true}));
    }

    hide() {
        this._el.classList.remove('tie-holder--visible');
        this._observer['afterhide'].forEach((cb) => cb());
    }

    on(eventName, cb) {
        this._observer[eventName].push(cb);
    }
}

export default TieHolder;
