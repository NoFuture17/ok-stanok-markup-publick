// phones__link-for-modal

import Alertino from '../../jubilant-engine/components/je-alertino/je-alertino';
import ToughGuy from '../../jubilant-engine/tough-guy/tough-guy';
import autosize from 'autosize';

let jeAlertino = new Alertino();
function initModal() {
    let links = document.querySelectorAll('.phones__link-for-modal, .link-for-modal');
    Array.from(links).forEach((link)=>{
        link.addEventListener('click', (e)=> {
            e.preventDefault();
            e.stopPropagation();
            // console.log('hell');
            if ('ontouchstart' in window) {
                //для малых
                let nodeToInsert = document.querySelector('.order-callback-modal-content-touch');//.cloneNode(true);
                jeAlertino.setContentNode(nodeToInsert);
                jeAlertino.setCssClassModifier('je-alertino--order-callback')
                jeAlertino.setCssClassBefore('order-callback-triangle-top');
                jeAlertino.setCssClassAfter('order-callback-triangle-bottom');
                jeAlertino.open();
            }
            else {
                //для больших
                let nodeToInsert = document.querySelector('.order-callback-modal-content-no-touch');//.cloneNode(true);
                autosize(nodeToInsert.querySelector('textarea'));
                jeAlertino.setContentNode(nodeToInsert);
                jeAlertino.setCssClassModifier('je-alertino--order-callback')
                jeAlertino.setCssClassBefore('order-callback-triangle-top');
                jeAlertino.setCssClassAfter('order-callback-triangle-bottom');
                jeAlertino.open();

                if (!nodeToInsert.dataset['toughguy']) {
                    nodeToInsert.dataset['toughguy'] = true;
                    let form = nodeToInsert.querySelector('form');
                    var toughGuy = new ToughGuy({
                        form: form,
                        validateBeforeSubmit: true,
                        onValid: () => {
                            var formData = new FormData(form);
                            //Сюда поставить правильный url обработчика
                            // fetch('/url', {
                            fetch(form.getAttribute('action'), {
                                method: 'POST',
                                body: formData,
                                credentials: 'same-origin', //send cookie for current domain
                                credentials: 'include',     //send cookie for CORS
                            }).then(function (response) {
                                return response.json();
                            }).then(function (data) {
                                if (data.status == 1) {
                                    jeAlertino.setContentHTML('<br><p>' + data.message + '</p><br>');
                                    setTimeout(()=> {
                                        jeAlertino.close();
                                    }, 2000);
                                    return true;
                                } else if (data.status == 0 && data.message) {
                                    jeAlertino.setContentHTML('<br><p>' + data.message + '</p><br>');
                                } else if (data.status == 0 && data.errors) {
                                    var verifyCodeInput = form.querySelector('[name="verifyCode"]');
                                    if (verifyCodeInput) {
                                        $('#' + verifyCodeInput.id + '-image').yiiCaptcha('refresh');
                                    }
                                    for (var key in data.errors) {
                                        var input = form.querySelector('[name="' + key + '"]');
                                        input.classList.add('tg-invalid');
                                        var errorContainer = input.nextSibling;
                                        errorContainer.classList.add('tg-error--open');
                                        errorContainer.classList.add('tg-error--visible');
                                        errorContainer.querySelector('.tg-error__content').innerText = data.errors[key];
                                    }
                                }
                            })
                        },
                        hints: {
                            name: {
                                patternMismatch: 'Минимальная длина: 3 символа.',
                                    valueMissing: 'Укажите Ваше имя.'
                            },
                            email: {
                                valueMissing: 'Укажите Ваш почтовый адрес.',
                                    patternMismatch: 'Укажите верный почтовый адрес.',
                            },

                            phone: {
                                valueMissing: 'Укажите Ваш телефон.',
                                    patternMismatch: 'Укажите верный телефон.',
                            },
                            question: {
                                errorSelector: 'например по умолчанию',
                                    valueMissing: 'Поле должно быть заполнено ё',
                                    typeMismatch: 'Значение должно быть например mail или url ё',
                                    patternMismatch: 'значение не подходит под паттерн ё',
                                    tooLong: 'превышена максимальная длина, ё',
                                    rangeOverflow: 'значение превышает максимально допустимое ё',
                                    rangeUnderflow: 'значение меньше чем минимально допустимое ё',
                                    stepMismatch: 'ошибка шага',
                            }
                        }
                    });
                }
            }

        })
    });

}

function header() {
    initModal();
    //сгенерить искусственное открытие
    // setTimeout(()=> {
    //     let link = document.querySelector('.phones__link-for-modal');
    //     var myE = document.createEvent("Event");
    //     myE.initEvent("click", true, true);
    //     link.dispatchEvent(myE);
    // }, 300)
}

export default header;
