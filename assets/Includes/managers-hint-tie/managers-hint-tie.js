import isNodeHover from '../../jubilant-engine/isElementHover';

class ManagersHintTie {
    constructor(){
        this._node = document.querySelector('.managers-hint-tie');
        this.status = 'visible';   //visible/hidden

        this._mouseleaveListener = this._mouseleaveListener.bind(this);
        this._mouseEnterListener = this._mouseEnterListener.bind(this);
        this.show = this.show.bind(this);

        this._node.addEventListener('mouseenter', this._mouseEnterListener);
        this._node.addEventListener('mouseleave', this._mouseleaveListener);
        this._node.addEventListener('click', this._clickListener.bind(this));
    }

    _mouseEnterListener(){
        this._node.classList.add('managers-hint-tie--hover');
    }
    _mouseleaveListener(){
        setTimeout(()=> {
            if (!isNodeHover(this._node)) {
                this._node.classList.remove('managers-hint-tie--hover');
            }
        }, 333);
    }
    _clickListener(e){
        e.stopPropagation();    //нужно для того чтобы managersHintSlider нормально работал
        this.status = this.status == 'visible' ? 'hidden' : 'visible'; 
    }
    show(){
        this.status = 'visible';
    }
    get status(){return this._status}
    set status(newVal){
        // console.log(newVal)
        if (newVal == 'hidden'){
            this._node.classList.add('managers-hint-tie--hidden')
            this._node.classList.remove('managers-hint-tie--hover');
            this._runListeners('hide')
        } else {
            this._node.classList.remove('managers-hint-tie--hidden')
        }
        this._status = newVal
        
    }
    
    //Used listeners:
    //show
    //observer pattern
    on(eventName, cb){
        if (!this._listeners) this._listeners = [];
        if (!this._listeners[eventName]) this._listeners[eventName] = [];
        this._listeners[eventName].push(cb);
    }
    _runListeners(eName){this._listeners[eName].forEach((cb)=>{cb();})}
    //observer pattern end
}
export default ManagersHintTie;

