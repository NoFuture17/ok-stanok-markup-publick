class TieToggler {
    constructor() {
        this._el = document.querySelector('.tie-toggler');
        this.hide = this.hide.bind(this);
        this._initObserver();
        this._attachListeners();

    }
    _attachListeners(){
        this._el.addEventListener('click', this.hide);
    }
    _initObserver(){
        this._observer = {
            "afterhide" : [],
        }
    };
    hide(){
        this._el.classList.add('tie-toggler--hidden');
        this._observer['afterhide'].forEach((cb) => cb());
    }
    show(){
        this._el.classList.remove('tie-toggler--hidden');
    }
    on(eventName, cb){
        this._observer[eventName].push(cb);
    }
}

export default TieToggler
