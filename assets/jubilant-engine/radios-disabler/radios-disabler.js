/*
* Usage:
 <input type="radio" name="state" value="state-new">
 <input type="radio" name="state" value="state-old" checked>
* 
* let wareHouseDisabler = new RadiosDisabler({nlRadios: this._el.querySelectorAll('input[name="state"]')});
* 
* */

import Radio from './radio'
class RadiosDisabler {
    constructor({nlRadios, nForm, cbUncheck}){
        this._radios = [];
        this._nForm = nForm;
        this._isUncheckBrutal = true;   //в дальнейшем этот параметр может добавиться к API для того чтобы очищать не все кнопки, а только те у которых нет аттрибута checked в html
        
        this._initRadios(nlRadios, cbUncheck);
        this._initForm();
    }
    
    _initForm() {
        this._nForm.addEventListener('reset', ()=>{
            this._isUncheckBrutal ? this.uncheckAllBrutal() : this.uncheckAll();
        });
    }
    _initRadios(nlRadios, cbUncheck){
        Array.from(nlRadios).forEach((nRadio)=>{
            let radio = new Radio({node: nRadio, radiosDisabler: this, cbUncheck: cbUncheck});
            this._radios.push(radio);
        });
    }
    uncheckAll(){
        this._radios.forEach((radio)=>{
            radio.uncheck({});
        });
    }
    uncheckAllBrutal(){
        this._radios.forEach((radio)=>{
            radio.uncheckBrutal();
        });
    }
}
export default RadiosDisabler;