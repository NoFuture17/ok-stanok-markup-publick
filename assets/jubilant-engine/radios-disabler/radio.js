
//Если checked="false" то браузер это понимает как true
class Radio {
    constructor({node, radiosDisabler, cbUncheck = ()=>{}}){
        this._n = node;
        this._rd = radiosDisabler;
        this._cbUncheck = cbUncheck;
    
        this._init();
    }
    
    _init(){
        (this._n.checked) ? this.check() : this.uncheck({});
        this._n.addEventListener('click', (e)=>{
            // e.preventDefault();  не удалять: напоминание. с этим нихера не работает
            this._isCheckedNow() ? this.uncheck({mode: 'visual'}) : this.check();
        })
    }
    
    _isCheckedNow(){
        let ret = this._n.dataset.checked == 'true' ? true : false;
        return ret;
    }
    check(){
        // console.log('checking');
        this._uncheckAll();
        
        this._n.checked = true;
        this._n.dataset.checked = true;
        this._n.dataset.checkedOnInit = true;
    }

    /**
     * 
     * @param mode - режим снятия. Необьязательный. На данный момент использутся режимы:
     *      visual - только визуальное исчезновение чекбокса. Инициализация и отмена всех чекбоксов перед установкой другого не в счет.
     */
    uncheck({mode = ''}){
        // console.log('unchecking')
        this._n.checked = false;
        this._n.dataset.checked = false;
        this._n.dataset.checkedOnInit = false;
        if (mode == 'visual') this._cbUncheck(this);
    }
    /**
     * используется для того чтобы отменить зачеканные кнопки даже если в html указан аттрибут checked
     */
    uncheckBrutal(){
        this.uncheck({});
        this._n.removeAttribute('checked');
    }
    
    _uncheckAll(){
        this._rd.uncheckAll();
    }


    
    
}
export default Radio;