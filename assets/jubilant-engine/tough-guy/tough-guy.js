import Enemy from './enemy';

class ToughGuy {
    constructor({form, hints = {}, onValid = () => {}, onInvalid = () =>{}, otherConds = [], validateBeforeSubmit = false,}) {
        //api props
        this._form = form;
        this._onValid = onValid;
        this._onInvalid = onInvalid;
        this._otherConds = otherConds;   //array of callbacks, that return boolean. Например проверить ReCaptcha
        this._validateBeforeSubmit = validateBeforeSubmit;

        //internal props
        this._qtySubmit = 0;
        this._enemies = [];

        this._initEnemies(hints);
        this._initForm();
    }

    /**
     * Создает обьекты Enemy из инпутов и добавляет их в массив this._enemies. .
     * @param hintsAll
     * @private
     */
    _initEnemies(hintsAll) {
        let inputs = this._form.querySelectorAll('input, textarea'); //достаточно ли видов инпутов?
        Array.from(inputs).forEach(input => {
            let enemy = new Enemy({
                el: input,
                toughGuy: this,
                hintTexts: hintsAll[input.name] || false
            });
            this._enemies.push(enemy);
        });
    }

    /**
     * При сабмите вызывает колбэки _onValid или _onInvalid. Увеличивает _qtySubmit. .
     * @private
     */
    _initForm() {
        this._form.addEventListener('submit', e => {
            e.preventDefault();
            e.stopPropagation();
            this._qtySubmit += 1;

            if (this.validate({react: true}))
                this._onValid()
            else
                this._onInvalid();
        });
    }

    /**
     * false если нельзя валидировать до submit и submit-ов еще не было. .
     * @returns {boolean}
     */
    canCheckBeDone() {
        var canCheckBeDone = true;

        if (!this._validateBeforeSubmit && this._qtySubmit < 1)
            canCheckBeDone = false;

        return canCheckBeDone;
    }

    /**
     * false если один и инпутов или дополнительных условий неверно заполнен. .
     * @param react
     * @returns {boolean}
     */
    validate({react = false}) {
        var isFormValid = true;

        this._enemies.forEach(enemy => {
            if (!enemy.checkValidity()) {
                if (react) enemy.lose();
                isFormValid = false;
            }
        });
        this._otherConds.forEach(cond => {
            if (!(cond.check())) {
                isFormValid = false;
                if (react) cond.onInvalid()
            }
        });

        return isFormValid;
    }
}

export default ToughGuy;
