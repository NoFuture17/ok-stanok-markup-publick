let xCurr = 1;
let yCurr = 1;

document.addEventListener('mousemove', function (e) {
    xCurr = e.clientX;
    yCurr = e.clientY;
});
function isElementHover(el) {
    var rect = el.getBoundingClientRect();
    if (xCurr >= rect.left && 
        xCurr <= rect.right &&
        yCurr >= rect.top && 
        yCurr <= rect.bottom) {
        return true
    } else {
        return false;
    }
}

export default isElementHover;