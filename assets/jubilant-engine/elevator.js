/*
нужен smoothscroll-polyfill. Обычно подключается глобально в файле graceful-degradation

у nButton в стилях должно быть opacity: 0;
transition: opacity 0.2s;

Usage:
new Elevator({
 nButton: document.querySelector('.elevator'),
 offsetTransparent: window.innerHeight / 2, 
 offsetSolid: window.innerHeight, 
});



 */

import _ from 'lodash';

class Elevator {
    constructor({
        nButton,
        offsetTransparent = 300,
        offsetSolid = 600,  //      >=  offsetTransparent 
        behaviour = 'smooth'    //smooth/auto
    }) {
        this._nButton = nButton;
        this._offsetTransparent = offsetTransparent;
        this._offsetSolid = offsetSolid;
        this._offsetGap = this._offsetSolid - this._offsetTransparent;
        this._behaviour = behaviour;

        this._nButton.addEventListener('click', this._clickListener.bind(this));
        window.addEventListener('scroll', _.debounce(this._scrollListener.bind(this), 50));
        
    }
    _clickListener(){
        window.scroll({top: 0, left: 0, behavior: this._behaviour});
    }
    _scrollListener(){
        let opacity;
        let pointerEvents = 'all';
        let off = window.pageYOffset;
        
        if (off >= this._offsetSolid) {
            opacity = 1
        }
        else if (off < this._offsetTransparent) {
            opacity = 0
            pointerEvents = 'none';
        }
        else {
            let offFromTrans = off - this._offsetTransparent;
            opacity = (offFromTrans / this._offsetTransparent);
            opacity = Math.round(opacity * 100) / 100;
        }
        // console.log(opacity);
        this._nButton.style.opacity = opacity;
        this._nButton.style.pointerEvents = pointerEvents;
    }
}
export default Elevator;