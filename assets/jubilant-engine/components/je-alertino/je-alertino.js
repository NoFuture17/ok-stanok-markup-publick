//Использование:
/*
 если нужен бордюр, то усатанавливать для .je-alertino__content-wrapper
 import jeAlertino from '../jubilant-engine/components/je-alertino/je-alertino';
 jeAlertino.setContentText('ставлю этот контент при закрытии, но можно и в любой момент');
 jeAlertino.open();
 jeAlertino.close();

адская хрень. Была разработанна специально чтобы скроллбар не залазил на окна с треугольниками.
лучше ей не пользоваться (глючновата), и пользоваться вместо нее например picoModal
а от черезчур экзотических дизайнерских идей касательно модальных окон лучше либо отказываться, 
либо ставить сроки в несколько парсек.  
 */
import Ps from 'perfect-scrollbar';
import _ from 'lodash';
import onElementResize from '../../onElementResize';
import createNodeFromHTMLString from '../../createNodeFromHTMLString';
import onSingleTransitionEnd from '../../onSingleTransitionEnd';

class Alertino {
    constructor() {
        this._timeAni = 450;
        this._template = '';
        this._component = '';
        this._modal = '';
        this._content = '';
        this._contentWrapper = '';
        this._globalWrapper = '';
        this._body = '';
        this._elementToScroll = '';
        this._lastY = false;
        this._isStartedOnWindow = false;
        this._isContentSettingNow = false;
        this._initTemplate();
        this._initBindingListenersToThis();
        this._initListenersForClose();
        this._initListenersForWindowResize();
        this._initPerfectScrollbar();
        this._initListenersForContentUpdate();
    }
    _updateContentWrapper() {
        let oldContentWrapperHeight = this._contentWrapper.offsetHeight;

        let newContentWrapperHeight =
            this._content.clientHeight +
            parseFloat(getComputedStyle(this._contentWrapper)['borderTopWidth'].replace('px', '')) +
            parseFloat(getComputedStyle(this._contentWrapper)['borderBottomWidth'].replace('px', ''));

        //Если получается больше экрана, то установить размером с экран
        let beforeHeight = this._before.offsetHeight;
        let afterHeight = this._after.offsetHeight;
        let screenHeight = document.documentElement.clientHeight;
        if ((newContentWrapperHeight + beforeHeight + afterHeight) > screenHeight) {
            newContentWrapperHeight = screenHeight - beforeHeight - afterHeight;
        }
        newContentWrapperHeight = newContentWrapperHeight + 'px';

        this._contentWrapper.style.height = newContentWrapperHeight;

        if ((oldContentWrapperHeight+'px') == newContentWrapperHeight) {
            Ps.update(this._elementToScroll);
        } else {
            onSingleTransitionEnd(this._contentWrapper, 'height', () => {
                Ps.update(this._elementToScroll);
            })
        }

    }
    _setContent(animate, setContentCB) {
        let self = this;
        if (!animate) {
            self._component.classList.add('je-alertino--no-transition');
            setContentCB();
            self._component.classList.remove('je-alertino--no-transition');
        } else {
            self._content.classList.add('je-alertino__content--transparent');
            onSingleTransitionEnd(self._content, 'opacity', () => {
                setContentCB();
                self._content.classList.remove('je-alertino__content--transparent');
            });
        }
    }

    _initTemplate() {
        this._template = `
            <div class="je-alertino">
                <div class="je-alertino__overlay"></div>
                    <div class="je-alertino__modal">
                    <div class="je-alertino__global-wrapper">
                        <div class="je-alertino__before"></div>
                        <div class="je-alertino__content-wrapper">
                            <div class="je-alertino__cross"></div>
                            <div class="je-alertino__content"></div>
                        </div>
                        <div class="je-alertino__after"></div>
                    </div>
                </div>
            </div>
            `,
            this._body = document.querySelector('body');
        this._component = createNodeFromHTMLString(this._template);
        this._modal = this._component.querySelector('.je-alertino__modal');
        this._contentWrapper = this._component.querySelector('.je-alertino__content-wrapper');
        this._globalWrapper = this._component.querySelector('.je-alertino__global-wrapper');
        this._content = this._component.querySelector('.je-alertino__content');
        this._before = this._component.querySelector('.je-alertino__before');
        this._after = this._component.querySelector('.je-alertino__after');

        this._body.appendChild(this._component);

        this._elementToScroll = this._contentWrapper;

    }

    _windowScrollLock() {
    window.addEventListener('wheel', this._processWindowWheel);
    window.addEventListener('touchstart', this._processWindowTouchstart);
    window.addEventListener('touchmove', this._processWindowTouchmove);
    window.addEventListener('touchend', this._processWindowTouchend);
}
    _windowScrollUnLock() {
    window.removeEventListener('wheel', this._processWindowWheel);
    window.removeEventListener('touchstart', this._processWindowTouchstart);
    window.removeEventListener('touchmove', this._processWindowTouchmove);
    window.removeEventListener('touchend', this._processWindowTouchend);

}

    _initBindingListenersToThis() {
    this._processWindowWheel = this._processWindowWheel.bind(this);
    this._processWindowTouchstart = this._processWindowTouchstart.bind(this);
    this._processWindowTouchmove = this._processWindowTouchmove.bind(this);
    this._processWindowTouchend = this._processWindowTouchend.bind(this);
}
    _processWindowWheel(e) {
    e.preventDefault();
    e.stopPropagation();
    let steps = 7, ms = 100, distanceMultiplier = 1;
    for (var i = 0; i < steps; i++) {
        setTimeout(() => {
            this._contentWrapper.scrollTop += (e.deltaY / steps * distanceMultiplier);
        }, ms * i * (1 / steps))
    }
}
    _processWindowTouchstart(e) {
    // e.preventDefault();  //его здесь быть не должно. Оставил ради напоминания.
    e.stopPropagation();
    this._isStartedOnWindow = true;
    this._lastY = e.touches[0].clientY;
}
    _processWindowTouchmove(e) {
    e.preventDefault();
    e.stopPropagation();
    if (this._isStartedOnWindow) {
        var currentY = e.touches[0].clientY;
        this._contentWrapper.scrollTop -= (currentY - this._lastY);
        this._lastY = currentY;
    }
}
    _processWindowTouchend(e) {
    // e.preventDefault();  //его здесь быть не должно. Оставил ради напоминания.
    e.stopPropagation();
    this._isStartedOnWindow = false;
}
    _initListenersForClose() {
        //Закрытие при клике по крестику
        this._modal.querySelector('.je-alertino__cross').addEventListener('click', this.close.bind(this));

        //Закрытие при клике по пустому месту:
        //Закрыть при клике по html
        document.documentElement.addEventListener('click', (e)=>{
            this.close();
        });
        //Но не закрывать при клике на модальном окне.
        this._modal.addEventListener('click', (e) => {
            e.stopImmediatePropagation();
        });

    }
    _initListenersForWindowResize() {
    //При смене разрешения/ориентации изменение размера должно быть мгновенным
    var isResizeInProgress = false;
    window.addEventListener('resize', e => {
        if (isResizeInProgress) return;
        isResizeInProgress = true;
        this._component.classList.add('je-alertino--no-transition');
    });
    //Перерисовка при ресайзе
    window.addEventListener('resize', _.debounce(e => {
        this._updateContentWrapper();
        this._component.classList.remove('je-alertino--no-transition');
        Ps.update(this._elementToScroll);
        isResizeInProgress = false;
    }, 150));
}
    _initPerfectScrollbar() {
    Ps.initialize(this._elementToScroll);
    //костыль для smooth-scroll. true - для того чтобы перехватить событие как можно раньше.
    this._content.addEventListener('wheel', this._processWindowWheel, true);
}
    _initListenersForContentUpdate() {
    onElementResize(this._content, () => {
        this._updateContentWrapper();
    });
}


    debug() {
        this._component.classList.add('je-alertino--debug');
    }
    open() {
        this._component.classList.add('je-alertino--open');
        this._windowScrollLock();
    }
    close() {
        this._windowScrollUnLock();
        this._component.classList.remove('je-alertino--open');

    }
    setCssClassModifier(className) {
        this._component.className = '';
        this._component.classList.add('je-alertino');
        this._component.classList.add(className);
    }
    setCssClassAfter(className) {
        //например для треугольников
        this._after.classList.add(className);
    }
    setCssClassBefore(className) {
        //например для треугольников
        this._before.classList.add(className);
    }
    setContentNode(contentNode, animate = false) {
        this._setContent(animate, () => {
            this._content.innerHTML = '';
            this._content.appendChild(contentNode);
        });
    }

    /**
     * Не удается вернуть this, потому-что функция заканчивает работу в колбэке.
     * @param contentHTML
     * @param animate default: false. True классно смотриться когда окно уже открыто и в нем изменяется контент
     * false подходит для установки контента прежде чем окно откроется
     */
    setContentHTML(contentText, animate = false) {
        this._setContent(animate, () => {
            this._content.innerHTML = contentText;
        });
    }
}

export default Alertino;
