//Usage
// import setEqualHeightToElements from 'jubilant-engine/setEqualHeightToElements';
// setTimeout(function(){
//     setEqualHeightToElements({selector: '.services-list p.name', breakpoint: 480});
//     setEqualHeightToElements({selector: '.services-list p.preview', breakpoint: 480});
// },1);

import _ from 'lodash';
let setEqualHeightToElements = function ({selector='.someSelector', breakpoint=0}) {
    let elementsToBeEqual = document.querySelectorAll(selector);

    function collate() {
        if (window.innerWidth >= breakpoint) {
            let maxHeight = 0;
            //getting max height
            Array.from(elementsToBeEqual).forEach(function (element) {
                element.style.height = 'auto';
                let elementHeight = element.clientHeight;
                maxHeight = maxHeight < elementHeight ? elementHeight : maxHeight;
            });
            //set max height
            Array.from(elementsToBeEqual).forEach(function (element) {
                element.style.height = maxHeight + 'px';
            });
        } else {
            Array.from(elementsToBeEqual).forEach(function (element) {
                element.style.height = 'auto';
            });
        }
    }

    collate();
    window.addEventListener('resize', _.debounce(collate, 150));
}


export default setEqualHeightToElements;
