//неопробована после дефолтовых аргументов. Проверить.
function getRandomString({length = '', chars = ''}) {
    length = length ? length : Math.floor(Math.random() * (30 - 3) + 3);
    chars = chars ? chars : '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var result = '';
    for (var i = length; i > 0; --i){
        result += chars[Math.round(Math.random() * (chars.length - 1))];
    }
    return result;
}

export default getRandomString;
