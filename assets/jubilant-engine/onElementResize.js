//используется в:
/*
je-alertino
*/

//Использование:
/*
import onElementResize from '../../onElementResize';
onElementResize(el, cb);
*/

let tickInterval = 50;     //ms
let records = [];
setInterval(()=> {
    records.forEach(record => {
        let currentScrollHeight = record.el.scrollHeight
        if (record.lastDimensions.scrollHeight != currentScrollHeight) {
            record.lastDimensions.scrollHeight = currentScrollHeight;
            record.cb();
        }
    })
}, tickInterval);

function onElementResize(el, cb) {
    records.push({
        el: el,
        cb: cb,
        lastDimensions: {
            scrollHeight: el.scrollHeight,
        }
    });
}

export default onElementResize;
