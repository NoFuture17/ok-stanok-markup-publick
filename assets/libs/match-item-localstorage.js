
//Внимание! class Matcher использует это-же хранилище данных
class MatchItemLocalStorage {
    constructor(matchItem){
        this._matchItem = matchItem;
    }
    
    
    remove(){
        let oldArr = this._getOldArr();
        let index = oldArr.indexOf(this._getItemId());
        if (index > -1){
            // console.log(oldArr);
            let newArr = oldArr;
            newArr.splice(index, 1);
            // console.log(newArr);
            this._setNewArr(newArr);
        }
    }
    add(){
        if (!this._isPresentInStorage()) {
            let newArr = this._getOldArr();
            newArr.push(this._getItemId());
            // console.log(newArr);
            this._setNewArr(newArr);
        }
    }
    check(){
        let res = this._isPresentInStorage() ? true : false;
        return res;
    }
    _isPresentInStorage(){
        let oldArr = this._getOldArr();
        let res = false;
        if (oldArr.indexOf(this._getItemId()) != -1) res = true;
        return res;
    }
    _setNewArr(arr){
        localStorage.setItem(this._getScope(), JSON.stringify(arr));
    }
    _getOldArr(){
        let oldVal = window.localStorage.getItem(this._getScope());
        (oldVal == null) ? oldVal = [] : oldVal = JSON.parse(oldVal);
        return oldVal;
    }
    _getItemId(){return this._matchItem.getItemId();}
    _getScope(){return 'matcher-' + this._matchItem.getScope();}
}

export default MatchItemLocalStorage;